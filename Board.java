import java.util.*;
public class Board{	
	private Random rng;		//random to get random values to create the board with random hidden walls.
	private Tile[][] grid;
	private int LengthOfBoard = 5;
	public Board(){
		this.rng = new Random();		//initializing random field.
		this.grid = new Tile[LengthOfBoard][LengthOfBoard];	//initializing the board with the the required length(5).
		for(int i = 0; i< this.grid.length; i++){
			int randInt = rng.nextInt(grid.length);
			for(int j = 0; j < grid.length; j++){
				this.grid[i][j]=Tile.Blank;		//sets all positions to blank
				if(j == randInt){		
					this.grid[i][j]=Tile.HIDDEN_WALL;		//sets a random position in the row as a hidden wall.
				}
			}
		}
	}
	public String toString(){		//used to rewrite the form in which the Board object is printed.
		String result = "";		
		for(int i = 0; i< this.grid.length; i++){
			for(int j = 0; j < grid.length; j++){
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	public int placeToken(int row, int col){
		if(!(row >= 0 && row < grid.length) || !(col >= 0 && col< grid.length)){	//if the position doesn't exist then the method returns -2
			return -2;
		}
		else if(this.grid[row][col] == Tile.Blank){		//if the position is blank it places the castle.
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){	//if theres is a hidden wall in the position, it shows the hidden wall and returns 1.
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else{		// returns -1 when the position is either a castle or a wall.
			return -1;
		}
	}
	
}




