import java.util.*;
public class BoardGameApp{
	public static void main(String[] args){
		runGame();
	}
	public static void runGame(){
		Scanner reader = new Scanner(System.in);	//scanner used to get the position of where the user wants to place their token
		Board board = new Board();					// creates a board
		System.out.println("Hello There!\nWelcome to my new game.");		//welcomes the user.
		int numCastles = 7;
		int turns = 1;		
		int maxTurns = 9;
		int noRemainingCastles = 0;
		while(numCastles > noRemainingCastles && turns < maxTurns){
			System.out.println("Turn " + turns);
			System.out.println(board);
			System.out.println("You have " + numCastles + " castles left to place.\n");
			
			System.out.print("Give a number for which row You want to place your token: ");
			int row = Integer.parseInt(reader.nextLine())-1;					// int row to store which row the user wants to place their toke.
			System.out.print("Give a number for which Column You want to place your token: ");
			int col = Integer.parseInt(reader.nextLine())-1;					// int col to store which row the user wants to place their toke.	
			System.out.println();
			
			int placerCheck = board.placeToken(row, col); //int placerCheck is used to store the int returned by the place token method
			while(placerCheck < 0){		//checks whether the position was invalid and asks the user to re-enter the position.
				System.out.println("The values you entered were invalid Please re-enter the row and column.");
				
				System.out.print("Give a number for which row You want to place your token: ");
				row = Integer.parseInt(reader.nextLine())-1;
				System.out.print("Give a number for which Column You want to place your token: ");
				col = Integer.parseInt(reader.nextLine())-1;
				System.out.println();
				placerCheck = board.placeToken(row, col);
			}
			if(placerCheck == 1){		//Checks if the user tried placing their token on a wall and lets them know.
				System.out.println("There was already a wall in that position.");
			}
			if(placerCheck == 0){		//Checks if the token was placed.
				System.out.println("The castle was successfully placed.");
				numCastles--;
			}
			turns++;
			System.out.println("----------------------------------------------------------------");
		}
		System.out.println(board);
		if(numCastles == 0){		//checks if the user ran out of castles(won) and informs the user whether they won or lost.
			System.out.print("You WON my game!!");
		}
		else{
			System.out.print("You LOST my game!!");
		}
	}
}